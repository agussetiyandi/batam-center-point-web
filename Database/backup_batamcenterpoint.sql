-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 26, 2020 at 02:01 PM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `backup_batamcenterpoint`
--

-- --------------------------------------------------------

--
-- Table structure for table `agent`
--

CREATE TABLE `agent` (
  `agent_id` smallint(6) NOT NULL,
  `agent_code` char(3) COLLATE latin1_general_ci NOT NULL,
  `agent_name` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `agent_address` varchar(150) COLLATE latin1_general_ci DEFAULT NULL,
  `agent_phone` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `agent_country` char(3) COLLATE latin1_general_ci NOT NULL,
  `agent_email` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `agent_contact` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `agent_info` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `date_inserted` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_modified` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci COMMENT='Daftar Agen';

--
-- Dumping data for table `agent`
--

INSERT INTO `agent` (`agent_id`, `agent_code`, `agent_name`, `agent_address`, `agent_phone`, `agent_country`, `agent_email`, `agent_contact`, `agent_info`, `date_inserted`, `date_modified`) VALUES
(22, 'CFS', 'CITRA ADIARTHA SHIPPING', '---', '---', 'IDN', 'shipping@berjayawaterfront.com.my, OPERATION@CITRAFERRY.COM', '---', 'CITRA', '0000-00-00 00:00:00', '2020-03-07 02:49:31'),
(24, 'SRI', 'PT. PINTAS SAMUDRA', 'BATAM', '', 'IDN', 'shipping@berjayawaterfront.com.my, pintasbook@gmail.com, pintassamudra@gmail.com', '', 'PINTAS', '2020-03-12 03:19:57', '2020-03-12 03:19:57'),
(25, 'WJE', 'INTERNATIONAL GOLDEN SHIPPING', 'KOMPLEK BANDAR MAS BLOK B NO 8 SEI PANAS BATAM', '0778473130', 'IDN', 'ajulianshah@gmail.com, bcigsd011@gmail.com, pptferry@gmail.com, bpsejahteraamin@gmail.com, pt.igsbat', 'DAYAT', 'WIDI', '2020-03-12 03:20:09', '2020-03-12 03:20:09'),
(26, 'PFC', 'PT LAUTAN SARANA NUSANTARA', 'MAHKOTA RAYA BLOK A-6 BATAM CENTRE', '0778 7483381', 'IND', 'lautan.manifest@gmail.com, emanifest@singaporecruise.com.sg, lautansarana.bc@gmail.com', '', 'LAUTAN SARANA', '2009-11-17 08:03:15', '2020-03-07 02:49:31'),
(28, 'DBA', 'DUTA BAHARI SENTOSA', 'KOMPLEK PASAR PENUIN CENTER BLOK L NO 8 BATAM', '', 'IND', 'operasi@ferryrrs.com, joko.sentosa68@gmail.com, dutabaharisamudra2019@gmail.com', 'KUSNI', 'DUTA BAHARI', '2011-05-21 04:43:33', '2020-03-07 02:49:31'),
(33, 'PFL', 'PT. P MEGAH JAYA SEJAHTERA', 'KOMP MAHKOTA RAYA BLOK A NO 6', '07787483381', 'IDN', 'operations.bc@majesticfastferry.com.sg, emanifest@singaporecruise.com.sg', '', 'PELAYARAN MEGAH JAYA SEJAHTERA', '2014-12-24 10:14:36', '2020-03-07 02:49:31'),
(38, 'ASM', 'PT. ANUGRAH SAMUDRA MAS', 'KOMP. TRIKARSA EKUALITA, RUKO PASIR PUTIH BLOK F NO 3A', '0778 4883317 / 48831', 'IDN', 'ptanugrahsamudramas@gmail.com, tobindomanifest@gmail.com,  asm.ferries@gmail.com, tobindomanifest@ya', '---', '', '2019-08-31 14:36:18', '2020-03-07 02:49:31'),
(41, 'EE', 'BTGFGFGFGFG', 'RERD', '434343', 'VFD', 'RE@GMAIL.COM', 'FD', 'ERER', '2020-03-21 03:51:22', '2020-03-21 03:51:22');

-- --------------------------------------------------------

--
-- Table structure for table `destination`
--

CREATE TABLE `destination` (
  `destination_id` int(11) NOT NULL,
  `destination_name` varchar(255) NOT NULL,
  `destination_iso` varchar(10) NOT NULL,
  `date_inserted` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `date_modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `destination`
--

INSERT INTO `destination` (`destination_id`, `destination_name`, `destination_iso`, `date_inserted`, `date_modified`) VALUES
(19, 'BATAM', 'BTH', '2020-03-26 02:47:54', '2020-03-26 02:47:54'),
(20, 'MALAYSIA', 'MLY', '2020-03-26 02:47:54', '2020-03-26 02:47:54');

-- --------------------------------------------------------

--
-- Table structure for table `owner`
--

CREATE TABLE `owner` (
  `owner_id` smallint(6) NOT NULL,
  `owner_code` char(3) COLLATE latin1_general_ci NOT NULL,
  `owner_name` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `owner_address` varchar(150) COLLATE latin1_general_ci DEFAULT NULL,
  `owner_phone` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `owner_country` char(3) COLLATE latin1_general_ci NOT NULL,
  `owner_email` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `owner_info` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `date_inserted` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `date_modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci COMMENT='Daftar Owner';

--
-- Dumping data for table `owner`
--

INSERT INTO `owner` (`owner_id`, `owner_code`, `owner_name`, `owner_address`, `owner_phone`, `owner_country`, `owner_email`, `owner_info`, `date_inserted`, `date_modified`) VALUES
(24, 'CFS', 'PT. MARINATAMA GEMANUSA', 'KOMPLEK RUKO CITRA MAS BLOK A6-7 BALOI BATAM', '0778450910', 'IDN', '---', 'PERUSAHAAN PELAYARAN NASIONAL', '0000-00-00 00:00:00', '2020-03-13 04:39:16'),
(25, 'PBG', 'PT. PRIMA BUANA GEMABAHARI', '---', '---', 'IDN', '---', '---', '0000-00-00 00:00:00', '2020-03-13 04:39:16'),
(26, 'SRI', 'PT. PINTAS SAMUDRA', '---', '---', 'IDN', '---', '', '0000-00-00 00:00:00', '2020-03-13 04:39:16'),
(4, 'BEW', 'PT. BAHTERA EMPAT WISESA', 'MARITIME SQUARE #02-07 S.P.I. BUILDING SINGAPORE 099255', '021 6781234', 'IDN', '', '', '0000-00-00 00:00:00', '2020-03-13 04:39:16'),
(5, 'BBS', 'PT BATAM FAST', 'JL. DUYUNG BLOK A NO 13 KOMP. ORCHID CENTER SEI JODOH BATAM INDONESIA', '0361 755 5678', 'GAB', '', 'MERGER WITH PT. FFI', '0000-00-00 00:00:00', '2020-03-13 04:39:16'),
(6, 'BBI', 'PT. BERLIAN BATAM INDAH', '88 JL IBRAHIM SULTAN , STULANG LAUT, 800300 JOHOR - MALAYSIA', '021 6781234', 'MYS', '', 'XXX', '0000-00-00 00:00:00', '2020-03-13 04:39:16'),
(7, 'CBN', 'PT. CITRA BAHARI NUSINDO', 'KOMPLEKS DESTULANG JALAN IBRAHIM SULTAN LAUT 80300 JOHOR BAHRU - MALAYSIA', '0361 755 5678', 'MHL', '', 'KOLOM KETERANGAN INI BISA DIISI DENGAN KETERANGAN MENGENAI KAPAL...', '0000-00-00 00:00:00', '2020-03-13 04:39:16'),
(8, 'DBS', 'PT. DUTA BAHARI SAKTI', 'KOMPLEK PUSAT BANDAR JOHOR BAHRU MALAYSIA', '021 6781234', 'IDN', '', '', '0000-00-00 00:00:00', '2020-03-13 04:39:16'),
(10, 'STL', 'PT. SINJORI TATA LAUT', 'KOMP. JAYA PUTRA BLOK B NO 1- 2 SEI BATAM 29432 INDONESIA', '021 6781234', 'MYS', '', '', '0000-00-00 00:00:00', '2020-03-13 04:39:16'),
(11, 'WJE', 'PT. INTERNATIONAL GOLDEN SHIPPING', 'MARITIME SQUARE 02-20/03-29, WORLD TRADE CENTER S(099253)', '0361 755 5678', 'SOM', '', '', '0000-00-00 00:00:00', '2020-03-13 04:39:16'),
(27, 'PFC', 'PT. LAUTAN SARANA NUSANTARA', 'MAHKOTA RAYA BLOK A-6 BATAM CENTRE', '0778 7483381', 'IND', '', '-', '2009-11-17 08:06:35', '2020-03-13 04:39:16'),
(28, 'IFC', 'PT. INDO FALCON', '-', '0778381218', 'IND', '-', '-', '2010-06-22 04:36:42', '2020-03-13 04:39:16'),
(29, 'DBA', 'PT. DUTA BAHARI SENTOSA', '', '', 'IND', '', '', '2011-05-21 09:45:52', '2020-03-13 04:39:16'),
(30, 'WAV', 'PT SINGA LAUT PERKASA', 'FERRY INTERNATIONAL BATAM CENTER', '0778467511', 'IDN', '', 'PERUSAHAAN PELAYARAN', '2012-11-13 11:59:48', '2020-03-13 04:39:16'),
(31, 'WJS', 'PT. KURNIA SENTOSA', 'KOMPLEK CITRA SUPERMALL BLOK B NO 5-6 HARBOURBAY', '0778 7415227', 'IDN', '', '', '2013-02-04 09:49:11', '2020-03-13 04:39:16'),
(32, 'KUS', 'PT. KURNIA SENTOSA', 'JL.GUDANG MINYAK NO.22 TANJUNG PINANG', '0771-313766', 'IDN', '', '', '2013-04-04 07:56:19', '2020-03-13 04:39:16'),
(33, 'SLP', 'PT. SINGA LAUT PERKASA', 'JL.IMAM BONJOL NAGOYA BATAM', '0778 432948', 'IDN', '', '', '2013-10-27 07:42:58', '2020-03-13 04:39:16'),
(34, 'PFL', 'MAJESTIC FAST FERRY PTE.LTD', 'KOMP MAHKOTA RAYA BLOK A NO 6 BATAM CENTER', '07787483381', 'IDN', '', 'PERUSAHAAN PELAYARAN NASIONAL', '2014-12-24 10:10:00', '2020-03-13 04:39:16'),
(35, 'LIM', 'PT. LAUTAN INTI MEGA', 'KOMPLEK VILLAND PARK NO. 16 BUKIT SENYUM', '0778429769', 'IDN', '', 'PERUSAHAAN PELAYARAN NASIONAL', '2015-06-05 12:06:47', '2020-03-13 04:39:16'),
(39, 'ABT', 'PT. ARENA BAHARI TIRTATAMA', 'JL. PELANTAR I NO 568 TANJUNG PINANG', '077121452', 'IDN', '', 'PERUSAHAAN PELAYARAN', '2017-03-20 13:07:34', '2020-03-13 04:39:16'),
(38, 'WJK', 'PT. WIDI JASA EKSPRESS', 'KOMPLEK CITRA SUPER MALL BLOK B NO.5-6, BATU AMPAR', '0778 7415227', 'IDN', '', '', '2016-11-28 04:54:22', '2020-03-13 04:39:16'),
(37, 'BEA', 'PT. BAHTERA EMPAT WISESA II', '', '', 'IDN', '', '', '2016-09-21 10:30:35', '2020-03-13 04:39:16'),
(40, 'ASM', 'PT. ANUGRAH SAMUDRA MAS', 'KOMP. TRIKARSA EKUALITA, RUKO PASIR PUTIH BLOK F NO 3A', '0778 4883317 / 48831', 'IDN', '', '', '2019-08-31 14:29:14', '2020-03-13 04:39:16');

-- --------------------------------------------------------

--
-- Table structure for table `passenger`
--

CREATE TABLE `passenger` (
  `passenger_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `passenger_name` varchar(255) NOT NULL,
  `passenger_id_number` varchar(255) NOT NULL,
  `passenger_gender` varchar(255) NOT NULL,
  `passenger_birth_place` varchar(255) NOT NULL,
  `passenger_birth_date` datetime NOT NULL,
  `passenger_nationality` varchar(255) NOT NULL,
  `passenger_issue_place` varchar(255) NOT NULL,
  `passenger_issue_date` datetime NOT NULL,
  `passenger_pass_expired` datetime NOT NULL,
  `date_inserted` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `date_modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `passenger`
--

INSERT INTO `passenger` (`passenger_id`, `user_id`, `passenger_name`, `passenger_id_number`, `passenger_gender`, `passenger_birth_place`, `passenger_birth_date`, `passenger_nationality`, `passenger_issue_place`, `passenger_issue_date`, `passenger_pass_expired`, `date_inserted`, `date_modified`) VALUES
(1, 4, 'ilham', '213123213', '', '', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-03-14 03:25:52', '2020-03-14 03:25:52'),
(2, 4, 'nopia', '3213123', '', '', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-03-14 03:25:52', '2020-03-14 03:25:52'),
(3, 4, 'siti', '2321320', '', '', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-03-14 03:25:52', '2020-03-14 03:25:52'),
(4, 4, 'sinop', '30103012301', '', '', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-03-14 03:25:52', '2020-03-14 03:25:52'),
(5, 4, 'sinoptu', '132123121240', '', '', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-03-14 03:25:52', '2020-03-14 03:25:52'),
(6, 8, 'Agus', '2021-07-15', '', '', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-03-14 03:40:53', '2020-03-14 03:40:53'),
(7, 8, 'Budi', '2020-11-07', '', '', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-03-14 04:10:52', '2020-03-14 04:10:52'),
(8, 8, 'SANti', '2020-11-07', '', '', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-03-14 04:11:12', '2020-03-14 04:11:12'),
(9, 8, 'SANti', '2020-11-07', '', '', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-03-14 04:11:24', '2020-03-14 04:11:24'),
(10, 8, 'YANDI', '2024-12-07', '', '', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-03-14 04:13:27', '2020-03-14 04:13:27'),
(11, 8, 'YANDI', '2024-12-07', '', '', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-03-14 04:13:50', '2020-03-14 04:13:50'),
(12, 8, 'AGUS', '2018-07-01', '', '', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-03-16 04:03:21', '2020-03-16 04:03:21'),
(13, 8, 'AGUS', '2018-07-01', '', '', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-03-16 04:04:51', '2020-03-16 04:04:51'),
(14, 8, 'AGUS', '2018-07-01', '', '', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-03-16 04:05:09', '2020-03-16 04:05:09');

-- --------------------------------------------------------

--
-- Table structure for table `port`
--

CREATE TABLE `port` (
  `port_id` int(11) NOT NULL,
  `destination_id` int(11) NOT NULL,
  `port_name` varchar(255) NOT NULL,
  `port_iso` varchar(10) NOT NULL,
  `date_inserted` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `date_modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `port`
--

INSERT INTO `port` (`port_id`, `destination_id`, `port_name`, `port_iso`, `date_inserted`, `date_modified`) VALUES
(1, 2, 'Halim Perdana Kusuma', 'HLP', '2020-03-26 02:49:02', '2020-03-26 02:49:02'),
(2, 2, 'Soekarno-Hatta', 'CGK', '2020-03-26 02:49:02', '2020-03-26 02:49:02'),
(3, 1, 'Husein Sastranegara', 'BDO', '2020-03-26 02:49:02', '2020-03-26 02:49:02'),
(4, 7, 'Achmad Yani', 'SRG', '2020-03-26 02:49:02', '2020-03-26 02:49:02'),
(5, 3, 'Juanda Airport', 'SUB', '2020-03-26 02:49:02', '2020-03-26 02:49:02'),
(6, 4, 'Adisumarmo Airport', 'SOC', '2020-03-26 02:49:02', '2020-03-26 02:49:02'),
(7, 6, 'Adisucipto', 'JOG', '2020-03-26 02:49:02', '2020-03-26 02:49:02'),
(8, 8, 'Sultan Iskandar Muda', 'BTJ', '2020-03-26 02:49:02', '2020-03-26 02:49:02'),
(9, 10, 'Radint Inten II Airport', 'TKG', '2020-03-26 02:49:02', '2020-03-26 02:49:02'),
(10, 5, 'Tunggul Wulung', 'CXP', '2020-03-26 02:49:02', '2020-03-26 02:49:02'),
(11, 9, 'Fatmawati-Soekarno Airport', 'BKS', '2020-03-26 02:49:02', '2020-03-26 02:49:02'),
(12, 13, 'STL LAUT', 'STL', '2020-03-26 02:49:02', '2020-03-26 02:49:02'),
(16, 19, 'BATAM CENTER', 'BTC', '2020-03-26 02:49:02', '2020-03-26 02:49:02'),
(17, 20, 'STULANG LAUT', 'STL', '2020-03-26 02:49:02', '2020-03-26 02:49:02'),
(18, 20, 'PUTRI HARBOUR', 'PHB', '2020-03-26 02:49:02', '2020-03-26 02:49:02'),
(19, 20, 'PASIR GUDANG', 'PSG', '2020-03-26 02:49:02', '2020-03-26 02:49:02');

-- --------------------------------------------------------

--
-- Table structure for table `reservation`
--

CREATE TABLE `reservation` (
  `reservation_id` int(10) NOT NULL,
  `reservation_code` varchar(255) NOT NULL,
  `passenger_id` int(11) NOT NULL,
  `reservation_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `seat_code` varchar(10) NOT NULL,
  `rute_id` int(11) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `date_inserted` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `date_modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reservation`
--

INSERT INTO `reservation` (`reservation_id`, `reservation_code`, `passenger_id`, `reservation_date`, `seat_code`, `rute_id`, `status`, `date_inserted`, `date_modified`) VALUES
(1, 'TOM28021', 1, '2018-02-28 01:50:54', '015', 6, 0, '2020-03-26 02:49:31', '2020-03-26 02:49:31'),
(2, 'TOM28022', 2, '2018-02-28 01:50:54', '030', 6, 0, '2020-03-26 02:49:31', '2020-03-26 02:49:31'),
(3, 'TOM28023', 3, '2018-02-28 02:10:04', '014', 6, 0, '2020-03-26 02:49:31', '2020-03-26 02:49:31'),
(4, 'TOM28024', 4, '2018-02-28 02:52:27', '014', 5, 0, '2020-03-26 02:49:31', '2020-03-26 02:49:31'),
(5, 'TOM28025', 5, '2018-02-28 02:52:27', '015', 5, 0, '2020-03-26 02:49:31', '2020-03-26 02:49:31'),
(14, 'BCP16031', 12, '2020-03-16 04:05:09', '002', 10, 0, '2020-03-26 02:49:31', '2020-03-26 02:49:31');

-- --------------------------------------------------------

--
-- Table structure for table `rute`
--

CREATE TABLE `rute` (
  `rute_id` int(11) NOT NULL,
  `rute_departure` datetime NOT NULL,
  `rute_from` varchar(50) NOT NULL,
  `rute_to` varchar(50) NOT NULL,
  `rute_arrival` datetime NOT NULL,
  `rute_price` varchar(13) NOT NULL,
  `id_transportation` varchar(13) NOT NULL,
  `date_inserted` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `date_modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rute`
--

INSERT INTO `rute` (`rute_id`, `rute_departure`, `rute_from`, `rute_to`, `rute_arrival`, `rute_price`, `id_transportation`, `date_inserted`, `date_modified`) VALUES
(9, '2020-03-17 09:00:00', '16', '19', '2020-03-17 10:00:00', '200000', '5', '2020-03-16 04:02:03', '2020-03-26 02:50:26'),
(10, '2020-03-17 10:00:00', '16', '17', '2020-03-17 11:00:00', '260000', '7', '2020-03-16 04:01:50', '2020-03-26 02:50:26');

-- --------------------------------------------------------

--
-- Table structure for table `ship`
--

CREATE TABLE `ship` (
  `ship_id` int(11) NOT NULL,
  `ship_img` varchar(100) NOT NULL,
  `ship_name` varchar(255) NOT NULL,
  `ship_code` varchar(13) NOT NULL,
  `ship_description` text DEFAULT NULL,
  `ship_seat_qty` int(3) DEFAULT NULL,
  `date_inserted` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `date_modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ship`
--

INSERT INTO `ship` (`ship_id`, `ship_img`, `ship_name`, `ship_code`, `ship_description`, `ship_seat_qty`, `date_inserted`, `date_modified`) VALUES
(5, 'AGN-IGS.png', 'MV. MDM EXPRESS 02', 'MD02', '', 163, '2020-03-26 02:47:27', '2020-03-26 02:47:27'),
(6, 'AGN-IGS1.png', 'MV CITRA INDAH 99', 'CI99', '', 148, '2020-03-26 02:47:27', '2020-03-26 02:47:27'),
(7, 'AGN-SRI.png', 'MV. PINTAS SAMUDRA-9', 'PS09', '', 156, '2020-03-26 02:47:27', '2020-03-26 02:47:27'),
(8, 'AGN-SRI1.png', 'MV MIRANGGA ALPHA', 'MRAL', '', 155, '2020-03-26 02:47:27', '2020-03-26 02:47:27'),
(9, '', 'MV. DOLPHIN 3', 'AMS3', '', 105, '2020-03-26 02:47:27', '2020-03-26 02:47:27'),
(10, 'AGN-BES.png', 'MV. INDOMASTER 3', 'IDM3', '', 178, '2020-03-26 02:47:27', '2020-03-26 02:47:27'),
(11, 'AGN-BES1.png', 'CITRA INDOMAS', 'CI18', '', 89, '2020-03-26 02:47:27', '2020-03-26 02:47:27');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_full_name` varchar(100) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_name` varchar(32) NOT NULL,
  `user_password` varchar(64) NOT NULL,
  `user_phone` varchar(20) DEFAULT NULL,
  `user_gender` varchar(10) DEFAULT NULL,
  `level` int(11) NOT NULL,
  `date_inserted` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `date_modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_full_name`, `user_email`, `user_name`, `user_password`, `user_phone`, `user_gender`, `level`, `date_inserted`, `date_modified`) VALUES
(1, 'Administrator', 'dev.batamcenterpoint@gmail.com', 'admin', 'e10adc3949ba59abbe56e057f20f883e', '088814214', 'L', 1, '2020-03-24 06:47:22', '2020-03-24 06:47:22'),
(8, 'User Test', 'user@example.com', 'user', 'e10adc3949ba59abbe56e057f20f883e', '0811', 'L', 0, '2020-03-24 06:47:22', '2020-03-24 06:47:22');

-- --------------------------------------------------------

--
-- Table structure for table `vessel`
--

CREATE TABLE `vessel` (
  `vessel_id` smallint(6) NOT NULL,
  `vessel_code` varchar(4) COLLATE latin1_general_ci NOT NULL,
  `vessel_name` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `vessel_seat` smallint(6) NOT NULL DEFAULT 0,
  `vessel_baggage` smallint(6) NOT NULL DEFAULT 0,
  `vessel_country` char(3) COLLATE latin1_general_ci NOT NULL DEFAULT 'IDN',
  `vessel_flag` char(3) COLLATE latin1_general_ci NOT NULL DEFAULT 'IDN',
  `vessel_info` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `owner_code` char(3) COLLATE latin1_general_ci NOT NULL,
  `agent_code` char(3) COLLATE latin1_general_ci NOT NULL,
  `date_inserted` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci COMMENT='Daftar Vessel';

--
-- Dumping data for table `vessel`
--

INSERT INTO `vessel` (`vessel_id`, `vessel_code`, `vessel_name`, `vessel_seat`, `vessel_baggage`, `vessel_country`, `vessel_flag`, `vessel_info`, `owner_code`, `agent_code`, `date_inserted`, `date_modified`) VALUES
(20, 'PS09', 'MV. PINTAS SAMUDRA-9', 156, 2500, 'IDN', 'IDN', '---', 'SRI', 'SRI', '0000-00-00 00:00:00', '2020-03-13 04:32:28'),
(11, 'WM08', 'SEA FLYTE', 211, 2500, 'IDN', 'IDN', '---', 'BBS', 'BBS', '0000-00-00 00:00:00', '2020-03-13 04:32:28'),
(47, 'IND3', 'INDOMAS- 3', 97, 5000, 'IDN', 'IDN', '', 'BEW', 'BES', '0000-00-00 00:00:00', '2020-03-13 04:32:28'),
(50, 'INB5', 'INDOBERLIAN-5', 110, 2000, 'IDN', 'IDN', '', 'BEW', 'BES', '0000-00-00 00:00:00', '2020-03-13 04:32:28'),
(51, 'INDO', 'INDO-1', 101, 5000, 'IDN', 'IDN', '', 'BEW', 'BES', '0000-00-00 00:00:00', '2020-03-13 04:32:28'),
(53, 'OCRA', 'OCEAN RAIDER', 333, 2000, 'SGP', 'SGP', '', 'BBS', 'BBS', '0000-00-00 00:00:00', '2020-03-13 04:32:28'),
(54, 'OCFL', 'OCEAN FLYTE', 196, 2000, 'SGP', 'SGP', '', 'BBS', 'BBS', '0000-00-00 00:00:00', '2020-03-13 04:32:28'),
(55, 'ASR2', 'ASEAN RAIDER II', 191, 2000, 'SGP', 'SGP', '', 'BBS', 'BBS', '0000-00-00 00:00:00', '2020-03-13 04:32:28'),
(65, 'DEX2', 'MV. DUMAI EXPRESS II', 181, 2000, 'MYS', 'IDN', '', 'CFS', 'CFS', '0000-00-00 00:00:00', '2020-03-13 04:32:28'),
(69, 'MRAL', 'MV MIRANGGA ALPHA', 155, 2000, 'IDN', 'IDN', '', 'SRI', 'SRI', '0000-00-00 00:00:00', '2020-03-13 04:32:28'),
(101, 'PSMD', 'PINTAS SAMUDRA 3', 90, 0, 'IDN', 'IDN', '-', 'CFS', 'CFS', '2008-06-01 03:50:16', '2020-03-13 04:32:28'),
(75, 'INDS', 'INDOMAS-1', 168, 2000, 'IDN', 'IDN', '', 'BEW', 'BES', '0000-00-00 00:00:00', '2020-03-13 04:32:28'),
(76, 'INB3', 'INDOBERLIAN-3', 120, 2000, 'IDN', 'IDN', '', 'BEW', 'BES', '0000-00-00 00:00:00', '2020-03-13 04:32:28'),
(77, 'JTF1', 'JET FLYTE I', 160, 2000, 'SGP', 'SGP', '', 'BBS', 'BBS', '0000-00-00 00:00:00', '2020-03-13 04:32:28'),
(78, 'ASR1', 'ASEAN RAIDER I', 191, 2000, 'SGP', 'SGP', '', 'BBS', 'BBS', '0000-00-00 00:00:00', '2020-03-13 04:32:28'),
(88, 'PS08', 'MV. PINTAS SAMUDERA 8', 111, 2000, 'IDN', 'IDN', '-', 'SRI', 'SRI', '0000-00-00 00:00:00', '2020-03-13 04:32:28'),
(153, 'CI89', 'MV. CITRA INDAH 89', 141, 0, 'IDN', 'IDN', '', 'CFS', 'CFS', '2011-10-19 08:11:46', '2020-03-13 04:32:28'),
(104, 'KIND', 'MV. INDOMAS 3', 97, 0, 'IDN', 'IDN', '', 'SRI', 'SRI', '2008-06-26 06:40:25', '2020-03-13 04:32:28'),
(107, 'KB89', 'MV. WIDI EXPRESS 3', 119, 0, 'IDN', 'IDN', '', 'WJE', 'WJE', '2008-12-01 02:26:50', '2020-03-13 04:32:28'),
(112, 'CLN3', 'MV. CITRA LINE 3', 96, 0, 'IDN', 'IDN', '-', 'CFS', 'CFS', '2009-02-18 02:15:29', '2020-03-13 04:32:28'),
(122, 'BT18', 'BATAMFAST 18', 250, 0, 'IDN', 'SGP', '', 'BBS', 'BBS', '2009-11-19 10:16:20', '2020-03-13 04:32:28'),
(228, 'MVBL', 'MV BAHTERA LINGGA', 106, 0, 'IDN', 'IDN', '', 'SRI', 'SRI', '2016-01-20 01:29:22', '2020-03-13 04:32:28'),
(129, 'CR1', 'MV. CERIA INDOMAS', 143, 0, 'IDN', 'IDN', '', 'CFS', 'CFS', '2010-04-06 06:18:01', '2020-03-13 04:32:28'),
(136, 'KB90', 'MV. WIDI EXPRESS 15', 112, 0, 'IDN', 'IDN', '', 'WJE', 'WJE', '2010-11-23 00:12:37', '2020-03-13 04:32:28'),
(138, 'ALY1', 'MV. ALLYA EXPRESS 1', 104, 0, 'IDN', 'IDN', '', 'DBA', 'DBA', '2011-05-21 09:47:00', '2020-03-13 04:32:28'),
(141, 'IDM3', 'MV. INDOMASTER 3', 178, 0, 'IDN', 'IDN', '', 'BEW', 'BES', '2011-06-21 06:00:28', '2020-03-13 04:32:28'),
(157, 'MR8', 'MV MARINA EXPRESS 8', 110, 0, 'IDN', 'IDN', '', 'DBA', 'DBA', '2012-02-14 01:40:05', '2020-03-13 04:32:28'),
(159, 'SD31', 'MV. SINDO 31', 200, 0, 'IDN', 'SGP', '', 'PFC', 'PFC', '2012-02-29 22:49:10', '2020-03-13 04:32:28'),
(160, 'CI99', 'MV CITRA INDAH 99', 148, 0, 'IDN', 'IDN', '', 'WJE', 'WJE', '2012-03-17 01:50:10', '2020-03-13 04:32:28'),
(161, 'MS10', 'MV. SINDO 10', 200, 0, 'IDN', 'SGP', '', 'PFC', 'PFC', '2012-04-04 02:32:51', '2020-03-13 04:32:28'),
(162, 'QS@', 'MV. QUEEN STAR 2', 266, 0, 'IDN', 'SGP', '', 'PFC', 'PFC', '2012-04-26 01:43:33', '2020-03-13 04:32:28'),
(163, 'SD12', 'MV. SINDO 12', 200, 0, 'IDN', 'SGP', '', 'PFC', 'PFC', '2012-04-26 13:29:08', '2020-03-13 04:32:28'),
(165, 'BT19', 'BATAMFAST 19', 194, 0, 'SGP', 'SGP', '', 'BBS', 'BBS', '2012-05-10 03:51:56', '2020-03-13 04:32:28'),
(166, 'MQS1', 'MV. QUEEN STAR 1', 266, 0, 'IDN', 'SGP', '', 'PFC', 'PFC', '2012-06-03 08:31:40', '2020-03-13 04:32:28'),
(167, 'BT20', 'BATAMFAST 20', 194, 0, 'IDN', 'SGP', '', 'BBS', 'BBS', '2012-06-13 10:18:51', '2020-03-13 04:32:28'),
(170, 'SDEM', 'MV. SINDO EMPRESS', 150, 0, 'IDN', 'SGP', '', 'PFC', 'PFC', '2012-09-11 02:09:08', '2020-03-13 04:32:28'),
(172, 'VVPP', 'MV. SINDO PRINCESS', 150, 0, 'IDN', 'IDN', '', 'PFC', 'PFC', '2012-10-05 13:59:32', '2020-03-13 04:32:28'),
(173, 'SD06', 'MV. SINDO 6', 200, 0, 'IDN', 'SGP', '', 'PFC', 'PFC', '2012-10-22 07:58:13', '2020-03-13 04:32:28'),
(174, 'ML01', 'MV. MARINA LINES', 188, 0, 'IDN', 'IDN', '', 'SRI', 'SRI', '2012-10-27 23:38:02', '2020-03-13 04:32:28'),
(175, 'SD07', 'MV. SINDO 7', 200, 0, 'IDN', 'SGP', '', 'PFC', 'PFC', '2012-11-05 23:37:08', '2020-03-13 04:32:28'),
(186, 'MS01', 'MV. SINDO 1', 220, 0, 'SGP', 'SGP', '', 'PFC', 'PFC', '2012-11-21 12:26:06', '2020-03-13 04:32:28'),
(187, 'SD09', 'MV. SINDO 9', 149, 0, 'IDN', 'SGP', '', 'PFC', 'PFC', '2012-12-31 03:11:27', '2020-03-13 04:32:28'),
(192, 'IBN5', 'MV. INDOBERLIAN 5', 110, 0, 'IDN', 'IDN', '', 'SRI', 'SRI', '2013-02-23 23:03:50', '2020-03-13 04:32:28'),
(200, 'MVID', 'INDOMAS 1', 168, 0, 'IDN', 'IDN', '', 'CFS', 'CFS', '2013-08-23 06:48:16', '2020-03-13 04:32:28'),
(202, 'MIDM', 'INDOMAS 3', 97, 0, 'IDN', 'IDN', '', 'CFS', 'CFS', '2013-09-20 00:41:10', '2020-03-13 04:32:28'),
(204, 'MQS3', 'MV. QUEEN STAR 3', 300, 0, 'IDN', 'SGP', '', 'PFC', 'PFC', '2013-12-05 01:14:50', '2020-03-13 04:32:28'),
(206, 'MSY', 'MV.SEA RAY', 72, 0, 'IDN', 'IDN', '', 'BEW', 'BES', '2014-02-20 12:39:40', '2020-03-13 04:32:28'),
(208, 'GMB', 'MV. GEMBIRA II', 168, 0, 'IDN', 'IDN', '', 'DBA', 'DBA', '2014-06-01 05:05:25', '2020-03-13 04:32:28'),
(209, 'INR3', 'INDOBERLIAN 3', 120, 0, 'IDN', 'IDN', '', 'CFS', 'CFS', '2014-10-10 00:31:05', '2020-03-13 04:32:28'),
(217, 'PFLW', 'MV. WAVEMASTER-3', 168, 0, 'IDN', 'SGP', '', 'PFL', 'PFL', '2015-04-03 22:59:10', '2020-03-13 04:32:28'),
(218, 'PFL5', 'MV. WAVEMASTER-5', 168, 0, 'IDN', 'SGP', '', 'PFL', 'PFL', '2015-04-03 23:01:30', '2020-03-13 04:32:28'),
(219, 'PFL6', 'MV. WAVEMASTER-6', 168, 0, 'IDN', 'SGP', '', 'PFL', 'PFL', '2015-04-03 23:02:13', '2020-03-13 04:32:28'),
(220, 'PFL7', 'MV. WAVEMASTER-7', 168, 0, 'IDN', 'SGP', '', 'PFL', 'PFL', '2015-04-03 23:02:47', '2020-03-13 04:32:28'),
(221, 'PFL8', 'MV. WAVEMASTER-8', 168, 0, 'IDN', 'SGP', '', 'PFL', 'PFL', '2015-04-03 23:03:27', '2020-03-13 04:32:28'),
(222, 'PFL9', 'MV. WAVEMASTER-9', 168, 0, 'IDN', 'SGP', '', 'PFL', 'PFL', '2015-04-03 23:05:20', '2020-03-13 04:32:28'),
(227, 'IBN3', 'MV. INDOBERLIAN 3', 120, 0, 'IDN', 'IDN', '', 'SRI', 'SRI', '2015-11-24 01:49:22', '2020-03-13 04:32:28'),
(230, 'MVMS', 'MV. MARINA SYAHPUTRA 1', 136, 0, 'IDN', 'IDN', '', 'WJE', 'WJE', '2016-03-31 03:48:43', '2020-03-13 04:32:28'),
(232, 'MG3', 'MV GEMBIRA 3', 163, 0, 'IDN', 'IDN', '', 'WJE', 'WJE', '2016-06-12 09:49:02', '2020-03-13 04:32:28'),
(233, 'MVM7', 'MV. MAJESTIC 7', 200, 0, 'IDN', 'SGP', '', 'PFL', 'PFL', '2016-07-01 13:22:33', '2020-03-13 04:32:28'),
(234, 'MVM8', 'MV. MAJESTIC 8', 200, 0, 'SGP', 'SGP', '', 'PFL', 'PFL', '2016-08-26 22:54:06', '2020-03-13 04:32:28'),
(236, 'MVQ5', 'MV. QUEEN STAR 5', 268, 0, 'SGP', 'SGP', '', 'PFC', 'PFC', '2016-09-08 02:04:12', '2020-03-13 04:32:28'),
(240, 'MVM9', 'MV. MAJESTIC 9', 200, 0, 'SGP', 'SGP', '', 'PFL', 'PFL', '2016-11-11 23:21:38', '2020-03-13 04:32:28'),
(242, 'MVQ6', 'MV. QUEEN STAR 6', 268, 0, 'SGP', 'SGP', '', 'PFC', 'PFC', '2016-12-22 00:49:02', '2020-03-13 04:32:28'),
(248, 'CFSI', 'INDOMAS-3', 97, 5000, 'IDN', 'IDN', 'RECONSILIASI', 'BEW', 'BES', '2017-04-28 06:45:00', '2020-03-13 04:32:28'),
(249, 'CLE3', 'MV. CITRA LINES 3', 96, 2000, 'IDN', 'IDN', '---', 'SRI', 'SRI', '2017-05-04 04:16:23', '2020-03-13 04:32:28'),
(251, 'MVMD', 'MV. MAJESTIC DREAM', 317, 0, 'SGP', 'SGP', '', 'PFL', 'PFL', '2017-06-09 08:48:07', '2020-03-13 04:32:28'),
(252, 'MCL3', 'MV CITRA LINES 3', 96, 0, 'IDN', 'IDN', '', 'BEW', 'BES', '2017-08-11 03:54:44', '2020-03-13 04:32:28'),
(253, 'MVQ8', 'MV. QUEEN STAR 8', 268, 0, 'SGP', 'SGP', '', 'PFC', 'PFC', '2017-10-28 07:13:16', '2020-03-13 04:32:28'),
(255, 'MVMP', 'MV. MAJESTIC PRIDE', 317, 0, 'SGP', 'SGP', '', 'PFL', 'PFL', '2017-12-04 23:10:20', '2020-03-13 04:32:28'),
(256, 'CI18', 'CITRA INDOMAS', 89, 0, 'IDN', 'IDN', '', 'BEW', 'BES', '2018-01-11 04:41:32', '2020-03-13 04:32:28'),
(258, 'MVMF', 'MV. MAJESTIC FAITH', 317, 0, 'SGP', 'SGP', '', 'PFL', 'PFL', '2018-04-27 10:08:28', '2020-03-13 04:32:28'),
(262, 'MVME', 'MV MDM EXPRESS', 208, 0, 'IDN', 'IDN', '', 'WJE', 'WJE', '2018-10-05 23:22:26', '2020-03-13 04:32:28'),
(263, 'MVCL', 'MV. CITRA LEGACY 3', 183, 0, 'IDN', 'IDN', '', 'CFS', 'CFS', '2018-10-18 04:26:51', '2020-03-13 04:32:28'),
(265, 'MVMW', 'MV. MAJESTIC WISDOM', 317, 0, 'SGP', 'SGP', '', 'PFL', 'PFL', '2018-10-30 07:29:31', '2020-03-13 04:32:28'),
(267, 'MVML', 'MV. MAJESTIC LIBERTY', 317, 0, 'SGP', 'SGP', '', 'PFL', 'PFL', '2019-01-09 23:22:23', '2020-03-13 04:32:28'),
(268, 'MVAF', 'MV. ASIANFAST 1', 257, 0, 'SGP', 'SGP', '', 'BBS', 'BBS', '2019-01-28 08:58:18', '2020-03-13 04:32:28'),
(270, 'MVMG', 'MV. MAJESTIC GRACE', 317, 0, 'SGP', 'SGP', '', 'PFL', 'PFL', '2019-05-03 09:09:26', '2020-03-13 04:32:28'),
(271, 'ASM1', 'MV. DOLPHIN 01', 168, 0, 'IDN', 'IDN', '', 'ASM', 'ASM', '2019-08-31 15:04:14', '2020-03-13 04:32:28'),
(272, 'ASM2', 'MV. DOLPHIN 2', 168, 0, 'IDN', 'IDN', '', 'ASM', 'ASM', '2019-08-31 15:05:00', '2020-03-13 04:32:28'),
(273, 'ASM3', 'MV. DOLPHIN 3', 105, 0, 'IDN', 'IDN', '', 'ASM', 'ASM', '2019-08-31 15:05:46', '2020-03-13 04:32:28'),
(274, 'MVMB', 'BRILLIANCE OF MAJESTIC', 322, 0, 'SGP', 'SGP', '', 'PFL', 'PFL', '2019-09-16 12:01:08', '2020-03-13 04:32:28'),
(275, 'ASM5', 'MV. DOLPHIN 5', 168, 0, 'IDN', 'IDN', '', 'ASM', 'ASM', '2019-10-05 08:43:40', '2020-03-13 04:32:28'),
(276, 'OCA7', 'MV. OCEANNA 7', 162, 0, 'IDN', 'IDN', '', 'BEW', 'BES', '2019-10-05 10:56:22', '2020-03-13 04:32:28'),
(277, 'PBS8', 'MV. PINTAS SAMUDERA_8', 111, 0, 'IDN', 'IDN', '', 'BEW', 'BES', '2019-12-03 04:50:46', '2020-03-13 04:32:28'),
(278, 'ALY3', 'MV. ALLYA EXPRESS 3', 168, 0, 'IDN', 'IDN', '', 'DBA', 'DBA', '2019-12-13 09:41:04', '2020-03-13 04:32:28'),
(279, 'PBS0', 'MV. PINTAS SAMUDRA_9', 156, 0, 'IDN', 'IDN', '', 'BEW', 'BES', '2019-12-21 05:42:01', '2020-03-13 04:32:28'),
(280, 'MD02', 'MV. MDM EXPRESS 02', 163, 0, 'IDN', 'IDN', '', 'WJE', 'WJE', '2020-02-06 09:00:09', '2020-03-13 04:32:28');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agent`
--
ALTER TABLE `agent`
  ADD PRIMARY KEY (`agent_id`),
  ADD UNIQUE KEY `tda_kode` (`agent_code`);

--
-- Indexes for table `destination`
--
ALTER TABLE `destination`
  ADD PRIMARY KEY (`destination_id`);

--
-- Indexes for table `owner`
--
ALTER TABLE `owner`
  ADD PRIMARY KEY (`owner_id`),
  ADD UNIQUE KEY `tdw_kode` (`owner_code`);

--
-- Indexes for table `passenger`
--
ALTER TABLE `passenger`
  ADD PRIMARY KEY (`passenger_id`);

--
-- Indexes for table `port`
--
ALTER TABLE `port`
  ADD PRIMARY KEY (`port_id`);

--
-- Indexes for table `reservation`
--
ALTER TABLE `reservation`
  ADD PRIMARY KEY (`reservation_id`),
  ADD UNIQUE KEY `reservation_code` (`reservation_code`);

--
-- Indexes for table `rute`
--
ALTER TABLE `rute`
  ADD PRIMARY KEY (`rute_id`);

--
-- Indexes for table `ship`
--
ALTER TABLE `ship`
  ADD PRIMARY KEY (`ship_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `email` (`user_email`);

--
-- Indexes for table `vessel`
--
ALTER TABLE `vessel`
  ADD PRIMARY KEY (`vessel_id`),
  ADD UNIQUE KEY `tdv_kode` (`vessel_code`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agent`
--
ALTER TABLE `agent`
  MODIFY `agent_id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `destination`
--
ALTER TABLE `destination`
  MODIFY `destination_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `owner`
--
ALTER TABLE `owner`
  MODIFY `owner_id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `passenger`
--
ALTER TABLE `passenger`
  MODIFY `passenger_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `port`
--
ALTER TABLE `port`
  MODIFY `port_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `reservation`
--
ALTER TABLE `reservation`
  MODIFY `reservation_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `rute`
--
ALTER TABLE `rute`
  MODIFY `rute_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `ship`
--
ALTER TABLE `ship`
  MODIFY `ship_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `vessel`
--
ALTER TABLE `vessel`
  MODIFY `vessel_id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=281;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
