<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Edit</title>
	<?php $this->load->view('admin/common/scatas'); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<?php $this->load->view('admin/common/header'); ?>
		<?php $this->load->view('admin/common/menu'); ?>
		<div class="content-wrapper">
		<section class="content-header">
        <h1>Edit Data Agent</h1>
      
      </section>
			<section class="content">
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Agent</h3>
						<?php foreach ($agent as $data){?>
						<form action="<?php echo base_url(). 'admin/agent/update/'.$data->agent_id; ?>" method="post" class="form-horizontal">
					</div>
						<div class="box-body">
							<div class="form-group">
								<label>Agent Code</label>
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-qrcode"></i>
									</div>
									<input name="agent" type="text" class="form-control" placeholder="Pesawat" value="<?= $data->agent_code ?>" required/>
								</div>
							</div>
							<div class="form-group">
								<label>Agent Name</label>
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-book"></i>
									</div>
									<input name="agent_name" type="text" class="form-control" value="<?= $data->agent_name ?>"  placeholder="Agent Name" required/>
								</div>
								<?php } ?>
							</div>
              <div class="form-group">
								<label>Address</label>
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-compass"></i>
									</div>
									<input name="destination" type="text" class="form-control" placeholder="Address" value="<?= $data->agent_address ?>" required/>
								</div>
							</div>
              <div class="form-group">
								<label>Phone </label>
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-phone"></i>
									</div>
									<input name="destination" type="text" class="form-control" placeholder="Phone" value="<?= $data->agent_phone ?>" required/>
								</div>
							</div>
              <div class="form-group">
								<label>Country</label>
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-flag"></i>
									</div>
									<input name="destination" type="text" class="form-control" placeholder="Country" value="<?= $data->agent_country ?>" required/>
								</div>
							</div>
              <div class="form-group">
								<label>Email</label>
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-envelope"></i>
									</div>
									<input name="destination" type="text" class="form-control" placeholder="Email" value="<?= $data->agent_email ?>" required/>
								</div>
							</div>
              <div class="form-group">
								<label>Contact</label>
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-address-book"></i>
									</div>
									<input name="destination" type="text" class="form-control" placeholder="Contact" value="<?= $data->agent_contact ?>" required/>
								</div>
							</div>
              <div class="form-group">
								<label>Info</label>
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-info-circle"></i>
									</div>
									<input name="destination" type="text" class="form-control" placeholder="Info" value="<?= $data->agent_info ?>" required/>
								</div>
							</div>
						<input type="submit" class="btn btn-primary" />
					</div>
				</form>
				</div>
			</section>


		</div>
		<div class="control-sidebar-bg"></div>
	</div>
	<?php $this->load->view('admin/common/scbawah'); ?>
</body>
</html>