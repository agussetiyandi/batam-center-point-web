<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Edit</title>
	<?php $this->load->view('admin/common/scatas'); ?>
</head>

<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<?php $this->load->view('admin/common/header'); ?>
		<?php $this->load->view('admin/common/menu'); ?>
		<div class="content-wrapper">
			<section class="content-header">
				<h1>Edit Data Vessel</h1>

			</section>
			<section class="content">
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Vessel</h3>
						<?php foreach ($t_vessel as $data){?>
						<form action="<?php echo base_url(). 'admin/vessel/update/'.$data->vessel_id; ?>" method="post"
							class="form-horizontal">
					</div>
					<div class="box-body">
						<div class="form-group">
							<label>Vessel Code</label>
							<div class="input-group">
								<div class="input-group-addon">
									<i class="fa fa-qrcode"></i>
								</div>
								<input name="vessel_code" type="text" class="form-control" placeholder=". . ."
									value="<?= $data->vessel_code ?>" required />
							</div>
						</div>
						<div class="form-group">
							<label>Vessel Name</label>
							<div class="input-group">
								<div class="input-group-addon">
									<i class="fa fa-book"></i>
								</div>
								<input name="vessel_name" type="text" class="form-control" value="<?= $data->vessel_name ?>"
									placeholder=". . ." required />
							</div>
							<?php } ?>
						</div>
						<div class="form-group">
							<label>Seat</label>
							<div class="input-group">
								<div class="input-group-addon">
									<i class="fa fa-compass"></i>
								</div>
								<input name="vessel_seat" type="text" class="form-control" placeholder=". . ."
									value="<?= $data->vessel_seat ?>" required />
							</div>
						</div>
						<div class="form-group">
							<label>Baggage </label>
							<div class="input-group">
								<div class="input-group-addon">
									<i class="fa fa-phone"></i>
								</div>
								<input name="vessel_baggage" type="text" class="form-control" placeholder=". . ."
									value="<?= $data->vessel_baggage ?>" required />
							</div>
						</div>
						<div class="form-group">
							<label>Country</label>
							<div class="input-group">
								<div class="input-group-addon">
									<i class="fa fa-flag"></i>
								</div>
								<input name="vessel_country" type="text" class="form-control" placeholder=". . ."
									value="<?= $data->vessel_country ?>" required />
							</div>
						</div>
						<div class="form-group">
							<label>Flag</label>
							<div class="input-group">
								<div class="input-group-addon">
									<i class="fa fa-envelope"></i>
								</div>
								<input name="vessel_flag" type="text" class="form-control" placeholder=". . ."
									value="<?= $data->vessel_flag ?>" required />
							</div>
						</div>
						<div class="form-group">
							<label>Info</label>
							<div class="input-group">
								<div class="input-group-addon">
									<i class="fa fa-address-book"></i>
								</div>
								<input name="vessel_info" type="text" class="form-control" placeholder=". . ."
									value="<?= $data->vessel_info ?>" />
							</div>
						</div>
						<div class="form-group">
							<label>Owner Code</label>
							<div class="input-group">
								<div class="input-group-addon">
									<i class="fa fa-info-circle"></i>
								</div>
								<input name="owner_code" type="text" class="form-control" placeholder=". . ."
									value="<?= $data->owner_code ?>" required />
							</div>
						</div>
						<div class="form-group">
							<label>Agent Code</label>
							<div class="input-group">
								<div class="input-group-addon">
									<i class="fa fa-info-circle"></i>
								</div>
								<input name="agent_code" type="text" class="form-control" placeholder=". . ."
									value="<?= $data->agent_code ?>" required />
							</div>
						</div>
						<input type="submit" class="btn btn-primary" />
					</div>
					</form>
				</div>
			</section>


		</div>
		<div class="control-sidebar-bg"></div>
	</div>
	<?php $this->load->view('admin/common/scbawah'); ?>
</body>

</html>