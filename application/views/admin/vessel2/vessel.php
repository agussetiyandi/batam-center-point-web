<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Vessel</title>
  <?php $this->load->view('admin/common/scatas'); ?>
</head>

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">
    <?php $this->load->view('admin/common/header'); ?>
    <?php $this->load->view('admin/common/menu'); ?>
    <div class="content-wrapper">
      <section class="content-header">
        <h1>Vessel</h1>
        <div class="row">
        </div>
      </section>
      <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">


              <div class="float-right d-none d-md-block">
                <div id="add-button">
                  <a href="<?= base_url('admin/vessel/add') ?>" class="btn btn-success mdi mdi-plus mr-2"> Create Vessel</a>
                </div>
              </div>


              <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th style="text-align:center" width="5%">No</th>
                      <th style="text-align:center" width="12%">Vessel Code</th>
                      <th style="text-align:center">Vessel Name</th>
                      <th style="text-align:center">Seat</th>
                      <th style="text-align:center">Baggage</th>
                      <th style="text-align:center">Flag</th>
                      <th style="text-align:center">Owner</th>
                      <th style="text-align:center" width="11%">Agent Code</th>
                      <th width="9%">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no = 1; ?>
                    <?php foreach ($t_vessel as $data){?>
                    <tr>
                      <td style="text-align:center"><?php  echo $no++; ?></td>
                      <td style="text-align:center"><?php echo $data->vessel_code ?></td>
                      <td><?php echo $data->vessel_name ?></td>
                      <td style="text-align:center"><?php echo $data->vessel_seat ?></td>
                      <td style="text-align:center"><?php echo $data->vessel_baggage ?> Kg</td>
                      <td style="text-align:center"><?php echo $data->vessel_flag ?></td>
                      <td style="text-align:center"><?php echo $data->owner_code ?></td>
                      <td style="text-align:center"><?php echo $data->agent_code ?></td>
                      <td width="9%">
                        <a type="button" href="<?php echo base_url('admin/vessel/edit/'.$data->vessel_id) ?>"
                          class="btn btn-default btn-sm"><span class="fa fa-pencil"></span></a>
                        <a type="button" href="<?php echo base_url('admin/vessel/del/'.$data->vessel_id) ?>" onclick="return confirm('Delete <?=$data->vessel_name ?> ?')"
                          class="btn btn-default btn-sm"><span class="fa fa-trash"></span></a>

                        
                      </td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
  </section>
  </div>
  <div class="control-sidebar-bg"></div>
  </div>
  <?php $this->load->view('admin/common/scbawah'); ?>
</body>

</html>