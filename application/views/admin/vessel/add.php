<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Create Transportation</title>
	<?php $this->load->view('admin/common/scatas'); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<?php $this->load->view('admin/common/header'); ?>
		<?php $this->load->view('admin/common/menu'); ?>
		<div class="content-wrapper">
			<section class="content-header">
				<h1>Create Data</h1>
				<form action="<?php echo base_url(). 'admin/ship/add_ship'; ?>" method="post" enctype="multipart/form-data">
				</section>
				<section class="content">
					<div class="box box-info">
						<div class="box-header with-border">
							<h3 class="box-title">Vessel</h3>
						</div>
						<div class="box-body">
							<div class="form-group">
								<label>Logo</label>
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-image"></i>
									</div>
									<input name="ship_img" type="file" class="form-control" required />
								</div>
							</div>
							<div class="form-group">
								<label>Vessel Name</label>
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-ship"></i>
									</div>
									<input name="ship_name" type="text" class="form-control" placeholder=" . . . " required/>
								</div>
							</div>
							<div class="form-group">
								<label>Vessel Code</label>
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-qrcode"></i>
									</div>
									<input name="ship_code" type="text" class="form-control" placeholder=" . . . " required/>
								</div>
							</div>
							<div class="form-group">
								<label>Vessel Info</label>
								<textarea name="ship_description" class="form-control" rows="3" placeholder=" . . . "></textarea>
							</div>
							<div class="form-group">
								<label>Seat Qty</label>
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-at"></i>
									</div>
									<input name="ship_seat_qty" type="text" class="form-control" placeholder=" . . . " required/>
								</div>
							</div>
							<input type="submit" class="btn btn-primary" />
						</div>
					</div>
				</section>
			</form> 
		</div>
		<div class="control-sidebar-bg"></div>
	</div>
	<?php $this->load->view('admin/common/scbawah'); ?>
</body>
</html>