<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Agent extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_admin');
		$this->m_admin->sesiku();
	}

  // Function View
	public function index()
	{
		$data['agent']=$this->m_admin->tampil_agent();
		// $data['jairport']=$this->db->query('SELECT COUNT(*) FROM airport A JOIN destination D WHERE A.id_destination=D.id AND D.id=4');
		$this->load->view('admin/agent/agent',$data);
	}

  // Function Add 
	function add()
	{
		$this->load->view('admin/agent/add');
	}

	function add_agent()
	{
		$agent_id = $this->input->post('agent_id');
		$agent_code = $this->input->post('agent_code');
    $agent_name = $this->input->post('agent_name');
    $agent_address = $this->input->post('agent_address');
    $agent_phone = $this->input->post('agent_phone');
    $agent_country = $this->input->post('agent_country');
    $agent_email = $this->input->post('agent_email');
    $agent_contact = $this->input->post('agent_contact');
    $agent_info = $this->input->post('agent_info');

		$data = array(
			'agent_id' => $agent_id,
			'agent_code' => $agent_code,
      'agent_name' => $agent_name,
      'agent_address' => $agent_address,
      'agent_phone' => $agent_phone,
      'agent_country' => $agent_country,
      'agent_email' => $agent_email,
      'agent_contact' => $agent_contact,
      'agent_info' => $agent_info,
		);
		$this->m_admin->add_agent($data,'agent');
		redirect('admin/agent');
	}

  // Function Edit 
	function edit($agent_id)
	{
		$where = array('agent_id' => $agent_id);
		$data['agent'] = $this->m_admin->edit_agent('agent',$where)->result();
		$this->load->view('admin/agent/edit',$data);
	}

	function update($agent_id){
		$agent_code = $this->input->post('agent_code');
    $agent_name = $this->input->post('agent_name');
    $agent_address = $this->input->post('agent_address');
    $agent_phone = $this->input->post('agent_phone');
    $agent_country = $this->input->post('agent_country');
    $agent_email = $this->input->post('agent_email');
    $agent_contact = $this->input->post('agent_contact');
    $agent_info = $this->input->post('agent_info');

		$data = array(
			'agent_code' => $agent_code,
      'agent_name' => $agent_name,
      'agent_address' => $agent_address,
      'agent_phone' => $agent_phone,
      'agent_country' => $agent_country,
      'agent_email' => $agent_email,
      'agent_contact' => $agent_contact,
      'agent_info' => $agent_info,
		);
		$this->m_admin->update_agent($agent_id,$data);
		redirect('admin/agent','refresh');
	}

  // Function Delete
	function del($agent_id)
	{
		$this->m_admin->hapus_agent($agent_id);
		redirect('admin/agent','refresh');
	}

}

/* End of file dashboard.php */
/* Location: ./application/controllers/dashboard.php */