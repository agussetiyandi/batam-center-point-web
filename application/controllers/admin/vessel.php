<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vessel extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_admin');
		$this->m_admin->sesiku();
		$this->load->helper(array('form', 'url'));
	}

	public function index()
	{
		$data['vessel']=$this->m_admin->tampil_vessel();
		$this->load->view('admin/vessel/vessel',$data);
	}

	// Add Vessel
	function add()
	{
		$this->load->view('admin/vessel/add', array('error' => ' ' ));
	}

	function add_vessel()
	{
		$vessel_id = $this->input->post('vessel_id');
		$config['upload_path']          = './assets/images/vessel/';
		// $config['allowed_types']        = 'png|jpg|PNG';
		$config['allowed_types']        = '*';
		$config['max_size']             = 2048;
		$config['max_width']            = 1024;
		$config['max_height']           = 768;	

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('vessel_img'))
		{
			$vessel_img = '';
			$error = array('error' => $this->upload->display_errors());
			$this->load->view('admin/vessel/add', $error);
		}
		else
		{
			$upload_data = $this->upload->data();
			$data = array('upload_data' => $upload_data);
			$vessel_img = $upload_data['file_name'];
		}

		$vessel_name = $this->input->post('vessel_name');
		$vessel_code = $this->input->post('vessel_code');
		$vessel_info = $this->input->post('vessel_info');
		$vessel_seat_qty = $this->input->post('vessel_seat_qty');

		$data = array(
			'vessel_id' => $vessel_id,
			'vessel_img' => $vessel_img,
			'vessel_name' => $vessel_name,
			'vessel_code' => $vessel_code,
			'vessel_info' => $vessel_info,
			'vessel_seat_qty' => $vessel_seat_qty,
			);
		$this->m_admin->add_vessel($data,'vessel');
		redirect('admin/vessel');
	}

	// Edit Vessel
	function edit($vessel_id)
	{
		$data['vessel'] = $this->m_admin->edit_vessel('vessel',$vessel_id)->result();
		$this->load->view('admin/vessel/edit',$data);
	}

	function update(){
		$config['upload_path']          = './assets/images/vessel/';
		$config['allowed_types']        = '*';
		$config['max_size']             = 2048;
		$config['max_width']            = 1024;
		$config['max_height']           = 768;	

		$this->load->library('upload', $config);
		$vessel_id = $this->input->post('vessel_id');
		$vessel_name = $this->input->post('vessel_name');
		$vessel_code = $this->input->post('vessel_code');
		$vessel_info = $this->input->post('vessel_info');
		$vessel_seat_qty = $this->input->post('vessel_seat_qty');
		if ( ! $this->upload->do_upload('vessel_img'))
		{
			$vessel_img = '';
			$error = array('error' => $this->upload->display_errors());
			$data = array(
			'vessel_id' => $vessel_id,
			'vessel_name' => $vessel_name,
			'vessel_code' => $vessel_code,
			'vessel_info' => $vessel_info,
			'vessel_seat_qty' => $vessel_seat_qty,
			);
		}
		else
		{
			$upload_data = $this->upload->data();
			$data = array('upload_data' => $upload_data);
			$vessel_img = $upload_data['file_name'];
			$data = array(
			'vessel_id' => $vessel_id,
			'vessel_img' => $vessel_img,
			'vessel_name' => $vessel_name,
			'vessel_code' => $vessel_code,
			'vessel_info' => $vessel_info,
			'vessel_seat_qty' => $vessel_seat_qty,
			);
		}
		$this->m_admin->update_vessel($vessel_id,$data);
		redirect('admin/vessel','refresh');
	}

	// Delete Vessel
	function del($vessel_id)
	{
		$this->m_admin->hapus_vessel($vessel_id);
		redirect('admin/vessel','refresh');
	}

}

/* End of file Vessel */
/* Location: ./application/controllers/vessel */