<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Members extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('m_admin');
		$this->m_admin->sesiku();
	}

	// View User
	public function index()
	{
		$data['users']=$this->m_admin->tampil_user();
		$this->load->view('admin/member/member',$data);
	}

	// Edit User
	function edit($user_id){
		$data['users'] = $this->m_admin->edit_transportation('users',$user_id)->result();
		$this->load->view('admin/member/edit',$data);
	}

	function update($user_id){
		$user_full_name = $this->input->post('user_full_name');
		$user_email = $this->input->post('user_email');
		$user_name = $this->input->post('user_name');
		$user_phone = $this->input->post('user_phone');
		$user_gender = $this->input->post('user_gender');

		$data = array(
			'user_full_name' =>$user_full_name,
			'user_email' =>$user_email,
			'user_name' =>$user_name,
			'user_phone' =>$user_phone,
			'user_gender' =>$user_gender,
		);
		$this->m_admin->update_destination($user_id,$data);
		redirect('admin/members','refresh');
	}

	public function hapus_user($user_id)
	{
		$this->m_admin->hapus_user($user_id);
		redirect('admin/member/member','refresh');
	}

}