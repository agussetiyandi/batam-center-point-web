<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Destinations extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_admin');
		$this->m_admin->sesiku();
		$this->load->helper(array('form', 'url'));
	}

	public function index()
	{
		$data['destination']=$this->m_admin->tampil_destination();
		// $data['jairport']=$this->db->query('SELECT COUNT(*) FROM airport A JOIN destination D WHERE A.id_destination=D.id AND D.id=4');
		$this->load->view('admin/destination/destination',$data);
	}

	// Add Destination
	function add()
	{
		$this->load->view('admin/destination/add');
	}

	function add_destination()
	{
		$destination_id = $this->input->post('destination_id');
		$config['upload_path']          = './assets/images/destination/';
		// $config['allowed_types']        = 'png|jpg|PNG';
		$config['allowed_types']        = '*';
		$config['max_size']             = 2048;
		$config['max_width']            = 1024;
		$config['max_height']           = 768;	

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('destination_img'))
		{
			$destination_img = '';
			$error = array('error' => $this->upload->display_errors());
			$this->load->view('admin/destination/add', $error);
		}
		else
		{
			$upload_data = $this->upload->data();
			$data = array('upload_data' => $upload_data);
			$destination_img = $upload_data['file_name'];
		}

		$destination_name = $this->input->post('destination_name');
		$destination_iso = $this->input->post('destination_iso');

		$data = array(
			'destination_id' => $destination_id,
			'destination_img' => $destination_img,
			'destination_name' => $destination_name,
			'destination_iso' => $destination_iso,
		);
		$this->m_admin->add_destination($data,'destination');
		redirect('admin/destinations');
	}

	// Edit Destination
	function edit($destination_id)
	{
		$where = array('destination_id' => $destination_id);
		$data['destination'] = $this->m_admin->edit_destination('destination',$where)->result();
		$this->load->view('admin/destination/edit',$data);
	}

	// Update Destination
	function update(){
		$config['upload_path']          = './assets/images/destination/';
		$config['allowed_types']        = '*';
		$config['max_size']             = 2048;
		$config['max_width']            = 1024;
		$config['max_height']           = 768;	

		$this->load->library('upload', $config);
		$destination_id = $this->input->post('destination_id');
		$destination_name = $this->input->post('destination_name');
		$destination_iso = $this->input->post('destination_code');
		if ( ! $this->upload->do_upload('destination_img'))
		{
			$destination_img = '';
			$error = array('error' => $this->upload->display_errors());
			$data = array(
			'destination_id' => $destination_id,
			'destination_name' => $destination_name,
			'destination_iso' => $destination_iso,
			);
		}
		else
		{
			$upload_data = $this->upload->data();
			$data = array('upload_data' => $upload_data);
			$destination_img = $upload_data['file_name'];
			$data = array(
			'destination_id' => $destination_id,
			'destination_img' => $destination_img,
			'destination_name' => $destination_name,
			'destination_iso' => $destination_iso,
			);
		}
	// function update($destination_id){
	// 	$destination_name = $this->input->post('destination_name');
	// 	$destination_iso = $this->input->post('destination_iso');

	// 	$data = array(
	// 		'destination_name' =>$destination_name,
	// 		'destination_iso' =>$destination_iso,
	// 	);
		$this->m_admin->update_destination($destination_id,$data);
		redirect('admin/destinations','refresh');
	}

	// Delete Destination
	function del($destination_id)
	{
		$this->m_admin->hapus_destination($destination_id);
		redirect('admin/destinations','refresh');
	}

}

/* End of file dashboard.php */
/* Location: ./application/controllers/dashboard.php */