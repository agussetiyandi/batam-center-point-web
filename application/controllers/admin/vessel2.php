<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vessel extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_admin');
		$this->m_admin->sesiku();
	}

  // Function View
	public function index()
	{
		$data['t_vessel']=$this->m_admin->tampil_vessel();
		// $data['jairport']=$this->db->query('SELECT COUNT(*) FROM airport A JOIN destination D WHERE A.id_destination=D.id AND D.id=4');
		$this->load->view('admin/vessel/vessel',$data);
	}

  // Function Add 
	function add()
	{
		$this->load->view('admin/vessel/add');
	}

	function add_vessel()
	{
		$vessel_id = $this->input->post('agent_id');
		$vessel_code = $this->input->post('vessel_code');
    $vessel_name = $this->input->post('vessel_name');
    $vessel_seat = $this->input->post('vessel_seat');
    $vessel_baggage = $this->input->post('vessel_baggage');
    $vessel_country = $this->input->post('vessel_country');
    $vessel_flag = $this->input->post('vessel_flag');
    $vessel_info = $this->input->post('vessel_info');
		$owner_code = $this->input->post('owner_code');
		$agent_code = $this->input->post('agent_code');

		$data = array(
			'vessel_id' => $vessel_id,
			'vessel_code' => $vessel_code,
      'vessel_name' => $vessel_name,
      'vessel_seat' => $vessel_seat,
      'vessel_baggage' => $vessel_baggage,
      'vessel_country' => $vessel_country,
      'vessel_flag' => $vessel_flag,
      'vessel_info' => $vessel_info,
			'owner_code' => $owner_code,
			'agent_code' => $agent_code,
		);
		$this->m_admin->add_vessel($data,'t_vessel');
		redirect('admin/vessel');
	}

  // Function Edit 
	function edit($vessel_id)
	{
		$where = array('vessel_id' => $vessel_id);
		$data['t_vessel'] = $this->m_admin->edit_vessel('t_vessel',$where)->result();
		$this->load->view('admin/vessel/edit',$data);
	}

	function update($vessel_id){
		$vessel_code = $this->input->post('vessel_code');
    $vessel_name = $this->input->post('vessel_name');
    $vessel_seat = $this->input->post('vessel_seat');
    $vessel_baggage = $this->input->post('vessel_baggage');
    $vessel_country = $this->input->post('vessel_country');
    $vessel_flag = $this->input->post('vessel_flag');
    $vessel_info = $this->input->post('vessel_info');
		$owner_code = $this->input->post('owner_code');
		$agent_code = $this->input->post('agent_code');

		$data = array(
			'vessel_code' => $vessel_code,
      'vessel_name' => $vessel_name,
      'vessel_seat' => $vessel_seat,
      'vessel_baggage' => $vessel_baggage,
      'vessel_country' => $vessel_country,
      'vessel_flag' => $vessel_flag,
      'vessel_info' => $vessel_info,
			'owner_code' => $owner_code,
			'agent_code' => $agent_code,
		);
		$this->m_admin->update_vessel($vessel_id,$data);
		redirect('admin/vessel','refresh');
	}

  // Function Delete
	function del($vessel_id)
	{
		$this->m_admin->hapus_vessel($vessel_id);
		redirect('admin/vessel','refresh');
	}

}

/* End of file dashboard.php */
/* Location: ./application/controllers/dashboard.php */